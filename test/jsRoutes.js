var jsRoutes = {}; (function(_root){
var _nS = function(c,f,b){var e=c.split(f||"."),g=b||_root,d,a;for(d=0,a=e.length;d<a;d++){g=g[e[d]]=g[e[d]]||{}}return g}
var _qS = function(items){var qs = ''; for(var i=0;i<items.length;i++) {if(items[i]) qs += (qs ? '&' : '') + items[i]}; return qs ? ('?' + qs) : ''}
var _s = function(p,s){return p+((s===true||(s&&s.secure))?'s':'')+'://'}
var _wA = function(r){return {ajax:function(c){c=c||{};c.url=r.url;c.type=r.method;return jQuery.ajax(c)}, method:r.method,type:r.method,url:r.url,absoluteURL: function(s){return _s('http',s)+'localhost:9000'+r.url},webSocketURL: function(s){return _s('ws',s)+'localhost:9000'+r.url}}}
_nS('controllers.Assets'); _root.controllers.Assets.at = 
      function(file) {
      return _wA({method:"GET", url:"/" + "assets/" + (function(k,v) {return v})("file", file)})
      }
   
_nS('controllers.AlunoCtrl'); _root.controllers.AlunoCtrl.simularTrocaSenha = 
      function(ids) {
      return _wA({method:"POST", url:"/" + "rest/alunos/senha" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("ids", ids)])})
      }
   
_nS('controllers.UnidadeCursoCtrl'); _root.controllers.UnidadeCursoCtrl.getAllUnidade = 
      function() {
      return _wA({method:"GET", url:"/" + "rest/unidade-curso/unidade"})
      }
   
_nS('controllers.UnidadeCursoCtrl'); _root.controllers.UnidadeCursoCtrl.salveUnidade = 
      function(codigo,nome) {
      return _wA({method:"POST", url:"/" + "rest/unidade-curso/unidade" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("codigo", codigo), (function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("nome", nome)])})
      }
   
})(jsRoutes)