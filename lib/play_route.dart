library play_route;
import "dart:js";

class Routing {
	static final none = new None();
	static Route generate(String controller, String action, {List params: null}) {
		var jsRoute = context['jsRoutes'];
		if(jsRoute == null){
			return null;
		}
		var controllerObject = jsRoute['controllers'][controller];
		if(controllerObject ==  null){
			return null;
		}
		var response;
	    if(params == null){
	      response = controllerObject.callMethod(action);
	    }else{
	      response = controllerObject.callMethod(action, params);
	    }
	    if(response != null)
			response = _createResponse(response);
		return response;

	}
	static Route asset(String relativeURL){
		return generate("Assets","at", params: [relativeURL]);
	}
	static Route _createResponse(dynamic jsResponse){
		Route route = new Route();
		route.method = jsResponse['method'];
		route.url = jsResponse['url'];
		route.type = jsResponse['type'];
		route.webSocketURL = jsResponse.callMethod("webSocketURL");
		route.absoluteURL = jsResponse.callMethod("absoluteURL");
		return route;
	}
}

class Route {
	String method;
	String type;
	String url;
	String webSocketURL;
	String absoluteURL;
	@override
	String toString() {
	  return url;
	}
}