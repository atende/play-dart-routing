import 'package:unittest/unittest.dart' hide expect;
import 'package:guinness/guinness.dart';
import 'package:play_route/play_route.dart';
main(){
	describe('A Routing class',(){
	  it('should generate a null route with no javascript object',(){
	  	var route = Routing.generate("dont","matter");
	  	expect(route, isNull);
	  });
	  
	  it('should generate a Route object when called a route with no params', (){
		var route = Routing.generate("UnidadeCursoCtrl","getAllUnidade");
		expect(route, isNotNull);
		expect(route.url, equals("/rest/unidade-curso/unidade"));
		expect(route.method, equals("GET"));
		expect(route.type, equals("GET"));
		expect(route.absoluteURL, equals("http://localhost:9000/rest/unidade-curso/unidade"));
		expect(route.webSocketURL, equals("ws://localhost:9000/rest/unidade-curso/unidade"));
	  });  

	  it('should generate a Route object when called a route with params', (){
		var route = Routing.generate("UnidadeCursoCtrl","salveUnidade",params: ["123","nome"]);
		expect(route, isNotNull);
		expect(route.url, equals("/rest/unidade-curso/unidade?codigo=123&nome=nome"));
		expect(route.method, equals("POST"));
		expect(route.type, equals("POST"));
		expect(route.absoluteURL, equals("http://localhost:9000/rest/unidade-curso/unidade?codigo=123&nome=nome"));
		expect(route.webSocketURL, equals("ws://localhost:9000/rest/unidade-curso/unidade?codigo=123&nome=nome"));
	  });

	  it('should generate assets url',(){
	  	var route = Routing.asset("asset.jpg");
	  	expect(route.url, equals("/assets/asset.jpg"));
	  	expect(route.method, equals("GET"));
	  });

	});
	describe('A Route class',(){
		it('should toString to the url',(){
			var route = Routing.generate("UnidadeCursoCtrl", "getAllUnidade");
			expect(route.toString(), equals("/rest/unidade-curso/unidade"));
		});
	});
}